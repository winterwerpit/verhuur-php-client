# OpenAPIClient-php

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)


## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$announcement_input_model = new \WinterwerpItVerhuurClient\Model\AnnouncementInputModel(); // \WinterwerpItVerhuurClient\Model\AnnouncementInputModel

try {
    $result = $apiInstance->announcementCreateAnnouncement($announcement_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementCreateAnnouncement: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AnnouncementApi* | [**announcementCreateAnnouncement**](docs/Api/AnnouncementApi.md#announcementcreateannouncement) | **POST** /v-api/v1/announcements | 
*AnnouncementApi* | [**announcementDeleteAnnouncement**](docs/Api/AnnouncementApi.md#announcementdeleteannouncement) | **DELETE** /v-api/v1/announcements/{announcementId} | 
*AnnouncementApi* | [**announcementGetAnnouncement**](docs/Api/AnnouncementApi.md#announcementgetannouncement) | **GET** /v-api/v1/announcements/{announcementId} | 
*AnnouncementApi* | [**announcementGetAnnouncements**](docs/Api/AnnouncementApi.md#announcementgetannouncements) | **GET** /v-api/v1/announcements | 
*AnnouncementApi* | [**announcementSwitchVisibility**](docs/Api/AnnouncementApi.md#announcementswitchvisibility) | **PATCH** /v-api/v1/announcements/{announcementId}/visibility | 
*AnnouncementApi* | [**announcementUpdateAnnouncement**](docs/Api/AnnouncementApi.md#announcementupdateannouncement) | **PUT** /v-api/v1/announcements/{announcementId} | 
*AuthenticationApi* | [**authenticationAuthenticate**](docs/Api/AuthenticationApi.md#authenticationauthenticate) | **POST** /v-api/v1/authentication | 
*BookingApi* | [**bookingCalculateAvailability**](docs/Api/BookingApi.md#bookingcalculateavailability) | **POST** /v-api/v1/bookings/availability | 
*BookingApi* | [**bookingCreateBooking**](docs/Api/BookingApi.md#bookingcreatebooking) | **POST** /v-api/v1/bookings | 
*BookingApi* | [**bookingDeleteBooking**](docs/Api/BookingApi.md#bookingdeletebooking) | **DELETE** /v-api/v1/bookings/{id} | 
*BookingApi* | [**bookingDeleteBookingByCode**](docs/Api/BookingApi.md#bookingdeletebookingbycode) | **DELETE** /v-api/v1/bookings/codes/{code} | 
*BookingApi* | [**bookingGetBookingByCode**](docs/Api/BookingApi.md#bookinggetbookingbycode) | **GET** /v-api/v1/bookings/codes/{code} | 
*BookingApi* | [**bookingGetContractByCode**](docs/Api/BookingApi.md#bookinggetcontractbycode) | **GET** /v-api/v1/bookings/codes/{code}/contract | 
*BookingApi* | [**bookingUpdateBookingTimesByCode**](docs/Api/BookingApi.md#bookingupdatebookingtimesbycode) | **PATCH** /v-api/v1/bookings/codes/{code}/times | 
*ConfigurationApi* | [**configurationClearCache**](docs/Api/ConfigurationApi.md#configurationclearcache) | **DELETE** /v-api/v1/configuration/cache | 
*CustomerApi* | [**customerRegister**](docs/Api/CustomerApi.md#customerregister) | **POST** /v-api/v1/customers | 
*DeskDeviceApi* | [**deskDeviceAddDeskDevice**](docs/Api/DeskDeviceApi.md#deskdeviceadddeskdevice) | **POST** /v-api/v1/deskDevices | 
*DeskDeviceApi* | [**deskDeviceDeleteDeskDevice**](docs/Api/DeskDeviceApi.md#deskdevicedeletedeskdevice) | **DELETE** /v-api/v1/deskDevices/{deskDeviceId} | 
*DeskDeviceApi* | [**deskDeviceGetDeskDevices**](docs/Api/DeskDeviceApi.md#deskdevicegetdeskdevices) | **GET** /v-api/v1/deskDevices | 
*ExtraApi* | [**extraCreateExtra**](docs/Api/ExtraApi.md#extracreateextra) | **POST** /v-api/v1/extras | 
*ExtraApi* | [**extraDeleteExtra**](docs/Api/ExtraApi.md#extradeleteextra) | **DELETE** /v-api/v1/extras/{id} | 
*ExtraApi* | [**extraGetExtra**](docs/Api/ExtraApi.md#extragetextra) | **GET** /v-api/v1/extras/{id} | 
*ExtraApi* | [**extraGetExtras**](docs/Api/ExtraApi.md#extragetextras) | **GET** /v-api/v1/extras | 
*ExtraApi* | [**extraUpdateExtra**](docs/Api/ExtraApi.md#extraupdateextra) | **PUT** /v-api/v1/extras/{id} | 
*FeatureFlagApi* | [**featureFlagGetFeatureFlagEnabled**](docs/Api/FeatureFlagApi.md#featureflaggetfeatureflagenabled) | **GET** /v-api/v1/featureFlags/{featureFlag} | 
*FileApi* | [**fileGetUpload**](docs/Api/FileApi.md#filegetupload) | **GET** /v-api/v1/files/{ownerId}/{filename} | 
*HealthCheckApi* | [**healthCheckPerformHealthCheck**](docs/Api/HealthCheckApi.md#healthcheckperformhealthcheck) | **GET** /v-api/v1/healthCheck | 
*MediaApi* | [**mediaCreateMediaItem**](docs/Api/MediaApi.md#mediacreatemediaitem) | **POST** /v-api/v1/media | 
*MediaApi* | [**mediaDeleteMediaItem**](docs/Api/MediaApi.md#mediadeletemediaitem) | **DELETE** /v-api/v1/media/{id} | 
*MediaApi* | [**mediaGetMediaItem**](docs/Api/MediaApi.md#mediagetmediaitem) | **GET** /v-api/v1/media/{id} | 
*MediaApi* | [**mediaUpdateMediaItem**](docs/Api/MediaApi.md#mediaupdatemediaitem) | **PUT** /v-api/v1/media/{id} | 
*MetadataApi* | [**metadataGetRentalItemMetadataDescriptions**](docs/Api/MetadataApi.md#metadatagetrentalitemmetadatadescriptions) | **GET** /v-api/v1/metadata/rentalItemMetadata/{objectType} | 
*MetadataApi* | [**metadataGetRentalItemTypeMetadataDescriptions**](docs/Api/MetadataApi.md#metadatagetrentalitemtypemetadatadescriptions) | **GET** /v-api/v1/metadata/rentalItemTypeMetadata/{objectType} | 
*PlaceApi* | [**placeCreatePlace**](docs/Api/PlaceApi.md#placecreateplace) | **POST** /v-api/v1/places | 
*PlaceApi* | [**placeDeletePlace**](docs/Api/PlaceApi.md#placedeleteplace) | **DELETE** /v-api/v1/places/{placeId} | 
*PlaceApi* | [**placeGetAll**](docs/Api/PlaceApi.md#placegetall) | **GET** /v-api/v1/places | 
*PlaceApi* | [**placeGetById**](docs/Api/PlaceApi.md#placegetbyid) | **GET** /v-api/v1/places/{placeId} | 
*PlaceApi* | [**placeGetByPermalink**](docs/Api/PlaceApi.md#placegetbypermalink) | **GET** /v-api/v1/places/permalink/{permalink} | 
*PlaceApi* | [**placeGetForUpdatingById**](docs/Api/PlaceApi.md#placegetforupdatingbyid) | **GET** /v-api/v1/places/{placeId}/editModel | 
*PlaceApi* | [**placeGetRentalItems**](docs/Api/PlaceApi.md#placegetrentalitems) | **GET** /v-api/v1/places/{placeId}/rentalItems | 
*PlaceApi* | [**placeUpdatePlace**](docs/Api/PlaceApi.md#placeupdateplace) | **PUT** /v-api/v1/places/{placeId} | 
*PlaceApi* | [**placeValidateOpeningTimes**](docs/Api/PlaceApi.md#placevalidateopeningtimes) | **POST** /v-api/v1/places/{placeId}/validateOpeningTimes | 
*PostcodeApi* | [**postcodeSearch**](docs/Api/PostcodeApi.md#postcodesearch) | **GET** /v-api/v1/postcodes/{postcode} | 
*QueuedMailApi* | [**queuedMailGetQueuedMail**](docs/Api/QueuedMailApi.md#queuedmailgetqueuedmail) | **GET** /v-api/v1/users/{userId}/queuedMails/{queuedMailId} | 
*QueuedMailApi* | [**queuedMailGetQueuedMailAttachment**](docs/Api/QueuedMailApi.md#queuedmailgetqueuedmailattachment) | **GET** /v-api/v1/users/{userId}/queuedMails/{queuedMailId}/attachments/{attachmentId} | 
*QueuedMailApi* | [**queuedMailGetQueuedMailAttachments**](docs/Api/QueuedMailApi.md#queuedmailgetqueuedmailattachments) | **GET** /v-api/v1/users/{userId}/queuedMails/{queuedMailId}/attachments | 
*QueuedMailApi* | [**queuedMailGetQueuedMailsOverview**](docs/Api/QueuedMailApi.md#queuedmailgetqueuedmailsoverview) | **GET** /v-api/v1/users/{userId}/queuedMails | 
*RentalItemApi* | [**rentalItemCreateRentalItem**](docs/Api/RentalItemApi.md#rentalitemcreaterentalitem) | **POST** /v-api/v1/rentalItems | 
*RentalItemApi* | [**rentalItemGetRentalItem**](docs/Api/RentalItemApi.md#rentalitemgetrentalitem) | **GET** /v-api/v1/rentalItems/{id} | 
*RentalItemApi* | [**rentalItemGetRentalItemForEditing**](docs/Api/RentalItemApi.md#rentalitemgetrentalitemforediting) | **GET** /v-api/v1/rentalItems/{id}/editModel | 
*RentalItemApi* | [**rentalItemGetRentalItems**](docs/Api/RentalItemApi.md#rentalitemgetrentalitems) | **GET** /v-api/v1/rentalItems | 
*RentalItemApi* | [**rentalItemUpdateRentalItem**](docs/Api/RentalItemApi.md#rentalitemupdaterentalitem) | **PUT** /v-api/v1/rentalItems/{id} | 
*RentalItemTypeApi* | [**rentalItemTypeCreateRentalItemType**](docs/Api/RentalItemTypeApi.md#rentalitemtypecreaterentalitemtype) | **POST** /v-api/v1/rentalItemTypes | 
*RentalItemTypeApi* | [**rentalItemTypeDeleteRentalItemType**](docs/Api/RentalItemTypeApi.md#rentalitemtypedeleterentalitemtype) | **DELETE** /v-api/v1/rentalItemTypes/{id} | 
*RentalItemTypeApi* | [**rentalItemTypeGetExtras**](docs/Api/RentalItemTypeApi.md#rentalitemtypegetextras) | **GET** /v-api/v1/rentalItemTypes/{id}/extras | 
*RentalItemTypeApi* | [**rentalItemTypeGetRentalItemPlaceIds**](docs/Api/RentalItemTypeApi.md#rentalitemtypegetrentalitemplaceids) | **GET** /v-api/v1/rentalItemTypes/{id}/places | 
*RentalItemTypeApi* | [**rentalItemTypeGetRentalItemType**](docs/Api/RentalItemTypeApi.md#rentalitemtypegetrentalitemtype) | **GET** /v-api/v1/rentalItemTypes/{id} | 
*RentalItemTypeApi* | [**rentalItemTypeGetRentalItemTypeDetails**](docs/Api/RentalItemTypeApi.md#rentalitemtypegetrentalitemtypedetails) | **GET** /v-api/v1/rentalItemTypes/{key}/details | 
*RentalItemTypeApi* | [**rentalItemTypeGetRentalItemTypes**](docs/Api/RentalItemTypeApi.md#rentalitemtypegetrentalitemtypes) | **GET** /v-api/v1/rentalItemTypes | 
*RentalItemTypeApi* | [**rentalItemTypeUpdateRentalItemType**](docs/Api/RentalItemTypeApi.md#rentalitemtypeupdaterentalitemtype) | **PUT** /v-api/v1/rentalItemTypes/{id} | 
*TariffApi* | [**tariffCalculateAmount**](docs/Api/TariffApi.md#tariffcalculateamount) | **POST** /v-api/v1/tariffs/{tariffId}/amount | 
*TariffApi* | [**tariffCalculateAmountByTariffName**](docs/Api/TariffApi.md#tariffcalculateamountbytariffname) | **POST** /v-api/v1/tariffs/{tariffName}/amount-by-tariff-name | 
*TariffApi* | [**tariffCreate**](docs/Api/TariffApi.md#tariffcreate) | **POST** /v-api/v1/tariffs | 
*TariffApi* | [**tariffDeleteTariff**](docs/Api/TariffApi.md#tariffdeletetariff) | **DELETE** /v-api/v1/tariffs/{tariffId} | 
*TariffApi* | [**tariffGetAll**](docs/Api/TariffApi.md#tariffgetall) | **GET** /v-api/v1/tariffs | 
*TariffApi* | [**tariffGetTariff**](docs/Api/TariffApi.md#tariffgettariff) | **GET** /v-api/v1/tariffs/{tariffId} | 
*TariffApi* | [**tariffUpdateTariff**](docs/Api/TariffApi.md#tariffupdatetariff) | **PUT** /v-api/v1/tariffs/{tariffId} | 
*TestingApi* | [**testingClearDates**](docs/Api/TestingApi.md#testingcleardates) | **POST** /v-api/v1/test/clearDates | 
*TestingApi* | [**testingEnsureUser**](docs/Api/TestingApi.md#testingensureuser) | **POST** /v-api/v1/test/ensureUser | 
*TestingApi* | [**testingGetRolesAndPermissions**](docs/Api/TestingApi.md#testinggetrolesandpermissions) | **GET** /v-api/v1/test/roles | 
*TestingApi* | [**testingGetTestContract**](docs/Api/TestingApi.md#testinggettestcontract) | **GET** /v-api/v1/test/contract/{bookingId} | 
*TestingApi* | [**testingRetrieveBooking**](docs/Api/TestingApi.md#testingretrievebooking) | **PATCH** /v-api/v1/test/retrieveBooking/{bookingId} | 
*TestingApi* | [**testingSetNow**](docs/Api/TestingApi.md#testingsetnow) | **POST** /v-api/v1/test/setNow | 
*TestingApi* | [**testingSetUtcNow**](docs/Api/TestingApi.md#testingsetutcnow) | **POST** /v-api/v1/test/setUtcNow | 
*UserApi* | [**userForgotPassword**](docs/Api/UserApi.md#userforgotpassword) | **PATCH** /v-api/v1/users/forgotPassword | 
*UserApi* | [**userGetUserInfo**](docs/Api/UserApi.md#usergetuserinfo) | **GET** /v-api/v1/users/userinfo | 
*UserApi* | [**userNewPassword**](docs/Api/UserApi.md#usernewpassword) | **PATCH** /v-api/v1/users/newPassword | 
*UserApi* | [**userUpdate**](docs/Api/UserApi.md#userupdate) | **PUT** /v-api/v1/users | 
*UserApi* | [**userUpdatePassword**](docs/Api/UserApi.md#userupdatepassword) | **PATCH** /v-api/v1/users/updatePassword | 

## Models

- [AmountCalculationInputModel](docs/Model/AmountCalculationInputModel.md)
- [AmountCalculationResultModel](docs/Model/AmountCalculationResultModel.md)
- [AnnouncementInputModel](docs/Model/AnnouncementInputModel.md)
- [AnnouncementModel](docs/Model/AnnouncementModel.md)
- [AnnouncementModelAnnouncementType](docs/Model/AnnouncementModelAnnouncementType.md)
- [AnnouncementType](docs/Model/AnnouncementType.md)
- [AvailabilityModel](docs/Model/AvailabilityModel.md)
- [BookingExtra](docs/Model/BookingExtra.md)
- [BookingExtraModel](docs/Model/BookingExtraModel.md)
- [BookingInputModel](docs/Model/BookingInputModel.md)
- [BookingSourceType](docs/Model/BookingSourceType.md)
- [BookingStatusType](docs/Model/BookingStatusType.md)
- [BriefBookingModel](docs/Model/BriefBookingModel.md)
- [CalculateAvailabilityInputModel](docs/Model/CalculateAvailabilityInputModel.md)
- [CreateBookingResultModel](docs/Model/CreateBookingResultModel.md)
- [CreateBookingResultModelBooking](docs/Model/CreateBookingResultModelBooking.md)
- [DateInputModel](docs/Model/DateInputModel.md)
- [DateRange](docs/Model/DateRange.md)
- [DayOfWeek](docs/Model/DayOfWeek.md)
- [DeskDeviceInputModel](docs/Model/DeskDeviceInputModel.md)
- [DeskDeviceModel](docs/Model/DeskDeviceModel.md)
- [ExtraInputModel](docs/Model/ExtraInputModel.md)
- [ExtraModel](docs/Model/ExtraModel.md)
- [ExtraModelTariff](docs/Model/ExtraModelTariff.md)
- [ExtraTariffLineModel](docs/Model/ExtraTariffLineModel.md)
- [ExtraTariffModel](docs/Model/ExtraTariffModel.md)
- [FeatureFlagResultModel](docs/Model/FeatureFlagResultModel.md)
- [ForgotPasswordModel](docs/Model/ForgotPasswordModel.md)
- [HealthCheckResultModel](docs/Model/HealthCheckResultModel.md)
- [LoginModel](docs/Model/LoginModel.md)
- [LoginResultModel](docs/Model/LoginResultModel.md)
- [MailSendStatusType](docs/Model/MailSendStatusType.md)
- [MediaItemInputModel](docs/Model/MediaItemInputModel.md)
- [MediaItemResultModel](docs/Model/MediaItemResultModel.md)
- [MediaItemUpdateInputModel](docs/Model/MediaItemUpdateInputModel.md)
- [MediaModel](docs/Model/MediaModel.md)
- [MetadataDescriptionModel](docs/Model/MetadataDescriptionModel.md)
- [MetadataFieldType](docs/Model/MetadataFieldType.md)
- [MetadataInputModel](docs/Model/MetadataInputModel.md)
- [MetadataListValueModel](docs/Model/MetadataListValueModel.md)
- [NewPasswordModel](docs/Model/NewPasswordModel.md)
- [OpeningTimeInputModel](docs/Model/OpeningTimeInputModel.md)
- [OpeningTimeModel](docs/Model/OpeningTimeModel.md)
- [OpeningTimeModelDayOfWeek](docs/Model/OpeningTimeModelDayOfWeek.md)
- [OpeningTimeValidationInputModel](docs/Model/OpeningTimeValidationInputModel.md)
- [OpeningTimeValidationResultModel](docs/Model/OpeningTimeValidationResultModel.md)
- [PlaceCategory](docs/Model/PlaceCategory.md)
- [PlaceInputModel](docs/Model/PlaceInputModel.md)
- [PlaceModel](docs/Model/PlaceModel.md)
- [PostcodeResult](docs/Model/PostcodeResult.md)
- [PostcodeWrapper](docs/Model/PostcodeWrapper.md)
- [PostcodeWrapperResource](docs/Model/PostcodeWrapperResource.md)
- [ProblemDetails](docs/Model/ProblemDetails.md)
- [QueuedMailAttachmentModel](docs/Model/QueuedMailAttachmentModel.md)
- [QueuedMailModel](docs/Model/QueuedMailModel.md)
- [QueuedMailOverviewModel](docs/Model/QueuedMailOverviewModel.md)
- [RegisterModel](docs/Model/RegisterModel.md)
- [RentalItemInputModel](docs/Model/RentalItemInputModel.md)
- [RentalItemMetadataModel](docs/Model/RentalItemMetadataModel.md)
- [RentalItemModel](docs/Model/RentalItemModel.md)
- [RentalItemModelRentalItemType](docs/Model/RentalItemModelRentalItemType.md)
- [RentalItemObjectType](docs/Model/RentalItemObjectType.md)
- [RentalItemTypeDetailsModel](docs/Model/RentalItemTypeDetailsModel.md)
- [RentalItemTypeInputModel](docs/Model/RentalItemTypeInputModel.md)
- [RentalItemTypeMetadataModel](docs/Model/RentalItemTypeMetadataModel.md)
- [RentalItemTypeModel](docs/Model/RentalItemTypeModel.md)
- [RentalItemTypePlaceModel](docs/Model/RentalItemTypePlaceModel.md)
- [TariffInputModel](docs/Model/TariffInputModel.md)
- [TariffLineInputModel](docs/Model/TariffLineInputModel.md)
- [TariffLineModel](docs/Model/TariffLineModel.md)
- [TariffModel](docs/Model/TariffModel.md)
- [UpdateBookingTimesInputModel](docs/Model/UpdateBookingTimesInputModel.md)
- [UpdatePasswordModel](docs/Model/UpdatePasswordModel.md)
- [UserModel](docs/Model/UserModel.md)
- [UserUpdateModel](docs/Model/UserUpdateModel.md)

## Authorization

Authentication schemes defined for the API:
### JWT

- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header


## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
    - Package version: `0.0.1`
    - Generator version: `7.4.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`

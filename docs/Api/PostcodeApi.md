# WinterwerpItVerhuurClient\PostcodeApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**postcodeSearch()**](PostcodeApi.md#postcodeSearch) | **GET** /v-api/v1/postcodes/{postcode} |  |


## `postcodeSearch()`

```php
postcodeSearch($postcode): \WinterwerpItVerhuurClient\Model\PostcodeWrapper
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PostcodeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$postcode = 'postcode_example'; // string

try {
    $result = $apiInstance->postcodeSearch($postcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostcodeApi->postcodeSearch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **postcode** | **string**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\PostcodeWrapper**](../Model/PostcodeWrapper.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

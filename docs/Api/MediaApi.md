# WinterwerpItVerhuurClient\MediaApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**mediaCreateMediaItem()**](MediaApi.md#mediaCreateMediaItem) | **POST** /v-api/v1/media |  |
| [**mediaDeleteMediaItem()**](MediaApi.md#mediaDeleteMediaItem) | **DELETE** /v-api/v1/media/{id} |  |
| [**mediaGetMediaItem()**](MediaApi.md#mediaGetMediaItem) | **GET** /v-api/v1/media/{id} |  |
| [**mediaUpdateMediaItem()**](MediaApi.md#mediaUpdateMediaItem) | **PUT** /v-api/v1/media/{id} |  |


## `mediaCreateMediaItem()`

```php
mediaCreateMediaItem($media_item_input_model): \WinterwerpItVerhuurClient\Model\MediaItemResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$media_item_input_model = new \WinterwerpItVerhuurClient\Model\MediaItemInputModel(); // \WinterwerpItVerhuurClient\Model\MediaItemInputModel

try {
    $result = $apiInstance->mediaCreateMediaItem($media_item_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->mediaCreateMediaItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **media_item_input_model** | [**\WinterwerpItVerhuurClient\Model\MediaItemInputModel**](../Model/MediaItemInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\MediaItemResultModel**](../Model/MediaItemResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `mediaDeleteMediaItem()`

```php
mediaDeleteMediaItem($id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $apiInstance->mediaDeleteMediaItem($id);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->mediaDeleteMediaItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `mediaGetMediaItem()`

```php
mediaGetMediaItem($id): \WinterwerpItVerhuurClient\Model\MediaModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->mediaGetMediaItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->mediaGetMediaItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\MediaModel**](../Model/MediaModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `mediaUpdateMediaItem()`

```php
mediaUpdateMediaItem($id, $media_item_update_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\MediaApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$media_item_update_input_model = new \WinterwerpItVerhuurClient\Model\MediaItemUpdateInputModel(); // \WinterwerpItVerhuurClient\Model\MediaItemUpdateInputModel

try {
    $apiInstance->mediaUpdateMediaItem($id, $media_item_update_input_model);
} catch (Exception $e) {
    echo 'Exception when calling MediaApi->mediaUpdateMediaItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |
| **media_item_update_input_model** | [**\WinterwerpItVerhuurClient\Model\MediaItemUpdateInputModel**](../Model/MediaItemUpdateInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

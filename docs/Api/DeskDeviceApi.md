# WinterwerpItVerhuurClient\DeskDeviceApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deskDeviceAddDeskDevice()**](DeskDeviceApi.md#deskDeviceAddDeskDevice) | **POST** /v-api/v1/deskDevices |  |
| [**deskDeviceDeleteDeskDevice()**](DeskDeviceApi.md#deskDeviceDeleteDeskDevice) | **DELETE** /v-api/v1/deskDevices/{deskDeviceId} |  |
| [**deskDeviceGetDeskDevices()**](DeskDeviceApi.md#deskDeviceGetDeskDevices) | **GET** /v-api/v1/deskDevices |  |


## `deskDeviceAddDeskDevice()`

```php
deskDeviceAddDeskDevice($desk_device_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\DeskDeviceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$desk_device_input_model = new \WinterwerpItVerhuurClient\Model\DeskDeviceInputModel(); // \WinterwerpItVerhuurClient\Model\DeskDeviceInputModel

try {
    $apiInstance->deskDeviceAddDeskDevice($desk_device_input_model);
} catch (Exception $e) {
    echo 'Exception when calling DeskDeviceApi->deskDeviceAddDeskDevice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **desk_device_input_model** | [**\WinterwerpItVerhuurClient\Model\DeskDeviceInputModel**](../Model/DeskDeviceInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deskDeviceDeleteDeskDevice()`

```php
deskDeviceDeleteDeskDevice($desk_device_id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\DeskDeviceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$desk_device_id = 56; // int

try {
    $apiInstance->deskDeviceDeleteDeskDevice($desk_device_id);
} catch (Exception $e) {
    echo 'Exception when calling DeskDeviceApi->deskDeviceDeleteDeskDevice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **desk_device_id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deskDeviceGetDeskDevices()`

```php
deskDeviceGetDeskDevices(): \WinterwerpItVerhuurClient\Model\DeskDeviceModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\DeskDeviceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->deskDeviceGetDeskDevices();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeskDeviceApi->deskDeviceGetDeskDevices: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\WinterwerpItVerhuurClient\Model\DeskDeviceModel[]**](../Model/DeskDeviceModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

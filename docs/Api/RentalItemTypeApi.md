# WinterwerpItVerhuurClient\RentalItemTypeApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**rentalItemTypeCreateRentalItemType()**](RentalItemTypeApi.md#rentalItemTypeCreateRentalItemType) | **POST** /v-api/v1/rentalItemTypes |  |
| [**rentalItemTypeDeleteRentalItemType()**](RentalItemTypeApi.md#rentalItemTypeDeleteRentalItemType) | **DELETE** /v-api/v1/rentalItemTypes/{id} |  |
| [**rentalItemTypeGetExtras()**](RentalItemTypeApi.md#rentalItemTypeGetExtras) | **GET** /v-api/v1/rentalItemTypes/{id}/extras |  |
| [**rentalItemTypeGetRentalItemPlaceIds()**](RentalItemTypeApi.md#rentalItemTypeGetRentalItemPlaceIds) | **GET** /v-api/v1/rentalItemTypes/{id}/places |  |
| [**rentalItemTypeGetRentalItemType()**](RentalItemTypeApi.md#rentalItemTypeGetRentalItemType) | **GET** /v-api/v1/rentalItemTypes/{id} |  |
| [**rentalItemTypeGetRentalItemTypeDetails()**](RentalItemTypeApi.md#rentalItemTypeGetRentalItemTypeDetails) | **GET** /v-api/v1/rentalItemTypes/{key}/details |  |
| [**rentalItemTypeGetRentalItemTypes()**](RentalItemTypeApi.md#rentalItemTypeGetRentalItemTypes) | **GET** /v-api/v1/rentalItemTypes |  |
| [**rentalItemTypeUpdateRentalItemType()**](RentalItemTypeApi.md#rentalItemTypeUpdateRentalItemType) | **PUT** /v-api/v1/rentalItemTypes/{id} |  |


## `rentalItemTypeCreateRentalItemType()`

```php
rentalItemTypeCreateRentalItemType($rental_item_type_input_model): \WinterwerpItVerhuurClient\Model\RentalItemTypeModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rental_item_type_input_model = new \WinterwerpItVerhuurClient\Model\RentalItemTypeInputModel(); // \WinterwerpItVerhuurClient\Model\RentalItemTypeInputModel

try {
    $result = $apiInstance->rentalItemTypeCreateRentalItemType($rental_item_type_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeCreateRentalItemType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **rental_item_type_input_model** | [**\WinterwerpItVerhuurClient\Model\RentalItemTypeInputModel**](../Model/RentalItemTypeInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemTypeModel**](../Model/RentalItemTypeModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeDeleteRentalItemType()`

```php
rentalItemTypeDeleteRentalItemType($id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $apiInstance->rentalItemTypeDeleteRentalItemType($id);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeDeleteRentalItemType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeGetExtras()`

```php
rentalItemTypeGetExtras($id): \WinterwerpItVerhuurClient\Model\ExtraModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->rentalItemTypeGetExtras($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeGetExtras: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\ExtraModel[]**](../Model/ExtraModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeGetRentalItemPlaceIds()`

```php
rentalItemTypeGetRentalItemPlaceIds($id): \WinterwerpItVerhuurClient\Model\RentalItemTypeDetailsModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->rentalItemTypeGetRentalItemPlaceIds($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeGetRentalItemPlaceIds: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemTypeDetailsModel**](../Model/RentalItemTypeDetailsModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeGetRentalItemType()`

```php
rentalItemTypeGetRentalItemType($id): \WinterwerpItVerhuurClient\Model\RentalItemTypeModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->rentalItemTypeGetRentalItemType($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeGetRentalItemType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemTypeModel**](../Model/RentalItemTypeModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeGetRentalItemTypeDetails()`

```php
rentalItemTypeGetRentalItemTypeDetails($key): \WinterwerpItVerhuurClient\Model\RentalItemTypeDetailsModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$key = 'key_example'; // string

try {
    $result = $apiInstance->rentalItemTypeGetRentalItemTypeDetails($key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeGetRentalItemTypeDetails: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **key** | **string**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemTypeDetailsModel**](../Model/RentalItemTypeDetailsModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeGetRentalItemTypes()`

```php
rentalItemTypeGetRentalItemTypes(): \WinterwerpItVerhuurClient\Model\RentalItemTypeModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->rentalItemTypeGetRentalItemTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeGetRentalItemTypes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemTypeModel[]**](../Model/RentalItemTypeModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemTypeUpdateRentalItemType()`

```php
rentalItemTypeUpdateRentalItemType($id, $rental_item_type_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemTypeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$rental_item_type_input_model = new \WinterwerpItVerhuurClient\Model\RentalItemTypeInputModel(); // \WinterwerpItVerhuurClient\Model\RentalItemTypeInputModel

try {
    $apiInstance->rentalItemTypeUpdateRentalItemType($id, $rental_item_type_input_model);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemTypeApi->rentalItemTypeUpdateRentalItemType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |
| **rental_item_type_input_model** | [**\WinterwerpItVerhuurClient\Model\RentalItemTypeInputModel**](../Model/RentalItemTypeInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

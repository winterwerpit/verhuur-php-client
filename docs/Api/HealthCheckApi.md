# WinterwerpItVerhuurClient\HealthCheckApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**healthCheckPerformHealthCheck()**](HealthCheckApi.md#healthCheckPerformHealthCheck) | **GET** /v-api/v1/healthCheck |  |


## `healthCheckPerformHealthCheck()`

```php
healthCheckPerformHealthCheck($x_health_check_key): \WinterwerpItVerhuurClient\Model\HealthCheckResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\HealthCheckApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$x_health_check_key = 'x_health_check_key_example'; // string

try {
    $result = $apiInstance->healthCheckPerformHealthCheck($x_health_check_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HealthCheckApi->healthCheckPerformHealthCheck: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **x_health_check_key** | **string**|  | [optional] |

### Return type

[**\WinterwerpItVerhuurClient\Model\HealthCheckResultModel**](../Model/HealthCheckResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

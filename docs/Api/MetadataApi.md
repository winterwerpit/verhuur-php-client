# WinterwerpItVerhuurClient\MetadataApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**metadataGetRentalItemMetadataDescriptions()**](MetadataApi.md#metadataGetRentalItemMetadataDescriptions) | **GET** /v-api/v1/metadata/rentalItemMetadata/{objectType} |  |
| [**metadataGetRentalItemTypeMetadataDescriptions()**](MetadataApi.md#metadataGetRentalItemTypeMetadataDescriptions) | **GET** /v-api/v1/metadata/rentalItemTypeMetadata/{objectType} |  |


## `metadataGetRentalItemMetadataDescriptions()`

```php
metadataGetRentalItemMetadataDescriptions($object_type): \WinterwerpItVerhuurClient\Model\MetadataDescriptionModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\MetadataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$object_type = new \WinterwerpItVerhuurClient\Model\RentalItemObjectType(); // RentalItemObjectType

try {
    $result = $apiInstance->metadataGetRentalItemMetadataDescriptions($object_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetadataApi->metadataGetRentalItemMetadataDescriptions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **object_type** | [**RentalItemObjectType**](../Model/.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\MetadataDescriptionModel[]**](../Model/MetadataDescriptionModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `metadataGetRentalItemTypeMetadataDescriptions()`

```php
metadataGetRentalItemTypeMetadataDescriptions($object_type): \WinterwerpItVerhuurClient\Model\MetadataDescriptionModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\MetadataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$object_type = new \WinterwerpItVerhuurClient\Model\RentalItemObjectType(); // RentalItemObjectType

try {
    $result = $apiInstance->metadataGetRentalItemTypeMetadataDescriptions($object_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MetadataApi->metadataGetRentalItemTypeMetadataDescriptions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **object_type** | [**RentalItemObjectType**](../Model/.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\MetadataDescriptionModel[]**](../Model/MetadataDescriptionModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

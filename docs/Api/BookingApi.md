# WinterwerpItVerhuurClient\BookingApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**bookingCalculateAvailability()**](BookingApi.md#bookingCalculateAvailability) | **POST** /v-api/v1/bookings/availability |  |
| [**bookingCreateBooking()**](BookingApi.md#bookingCreateBooking) | **POST** /v-api/v1/bookings |  |
| [**bookingDeleteBooking()**](BookingApi.md#bookingDeleteBooking) | **DELETE** /v-api/v1/bookings/{id} |  |
| [**bookingDeleteBookingByCode()**](BookingApi.md#bookingDeleteBookingByCode) | **DELETE** /v-api/v1/bookings/codes/{code} |  |
| [**bookingGetBookingByCode()**](BookingApi.md#bookingGetBookingByCode) | **GET** /v-api/v1/bookings/codes/{code} |  |
| [**bookingGetContractByCode()**](BookingApi.md#bookingGetContractByCode) | **GET** /v-api/v1/bookings/codes/{code}/contract |  |
| [**bookingUpdateBookingTimesByCode()**](BookingApi.md#bookingUpdateBookingTimesByCode) | **PATCH** /v-api/v1/bookings/codes/{code}/times |  |


## `bookingCalculateAvailability()`

```php
bookingCalculateAvailability($calculate_availability_input_model): \WinterwerpItVerhuurClient\Model\AvailabilityModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$calculate_availability_input_model = new \WinterwerpItVerhuurClient\Model\CalculateAvailabilityInputModel(); // \WinterwerpItVerhuurClient\Model\CalculateAvailabilityInputModel

try {
    $result = $apiInstance->bookingCalculateAvailability($calculate_availability_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingCalculateAvailability: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **calculate_availability_input_model** | [**\WinterwerpItVerhuurClient\Model\CalculateAvailabilityInputModel**](../Model/CalculateAvailabilityInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\AvailabilityModel[]**](../Model/AvailabilityModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `bookingCreateBooking()`

```php
bookingCreateBooking($booking_input_model): \WinterwerpItVerhuurClient\Model\CreateBookingResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$booking_input_model = new \WinterwerpItVerhuurClient\Model\BookingInputModel(); // \WinterwerpItVerhuurClient\Model\BookingInputModel

try {
    $result = $apiInstance->bookingCreateBooking($booking_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingCreateBooking: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **booking_input_model** | [**\WinterwerpItVerhuurClient\Model\BookingInputModel**](../Model/BookingInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\CreateBookingResultModel**](../Model/CreateBookingResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `bookingDeleteBooking()`

```php
bookingDeleteBooking($id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $apiInstance->bookingDeleteBooking($id);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingDeleteBooking: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `bookingDeleteBookingByCode()`

```php
bookingDeleteBookingByCode($code)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = 'code_example'; // string

try {
    $apiInstance->bookingDeleteBookingByCode($code);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingDeleteBookingByCode: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **code** | **string**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `bookingGetBookingByCode()`

```php
bookingGetBookingByCode($code): \WinterwerpItVerhuurClient\Model\BriefBookingModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = 'code_example'; // string

try {
    $result = $apiInstance->bookingGetBookingByCode($code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingGetBookingByCode: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **code** | **string**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\BriefBookingModel**](../Model/BriefBookingModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `bookingGetContractByCode()`

```php
bookingGetContractByCode($code)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = 'code_example'; // string

try {
    $apiInstance->bookingGetContractByCode($code);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingGetContractByCode: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **code** | **string**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `bookingUpdateBookingTimesByCode()`

```php
bookingUpdateBookingTimesByCode($code, $update_booking_times_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\BookingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = 'code_example'; // string
$update_booking_times_input_model = new \WinterwerpItVerhuurClient\Model\UpdateBookingTimesInputModel(); // \WinterwerpItVerhuurClient\Model\UpdateBookingTimesInputModel

try {
    $apiInstance->bookingUpdateBookingTimesByCode($code, $update_booking_times_input_model);
} catch (Exception $e) {
    echo 'Exception when calling BookingApi->bookingUpdateBookingTimesByCode: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **code** | **string**|  | |
| **update_booking_times_input_model** | [**\WinterwerpItVerhuurClient\Model\UpdateBookingTimesInputModel**](../Model/UpdateBookingTimesInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

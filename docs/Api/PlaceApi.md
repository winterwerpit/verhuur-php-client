# WinterwerpItVerhuurClient\PlaceApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**placeCreatePlace()**](PlaceApi.md#placeCreatePlace) | **POST** /v-api/v1/places |  |
| [**placeDeletePlace()**](PlaceApi.md#placeDeletePlace) | **DELETE** /v-api/v1/places/{placeId} |  |
| [**placeGetAll()**](PlaceApi.md#placeGetAll) | **GET** /v-api/v1/places |  |
| [**placeGetById()**](PlaceApi.md#placeGetById) | **GET** /v-api/v1/places/{placeId} |  |
| [**placeGetByPermalink()**](PlaceApi.md#placeGetByPermalink) | **GET** /v-api/v1/places/permalink/{permalink} |  |
| [**placeGetForUpdatingById()**](PlaceApi.md#placeGetForUpdatingById) | **GET** /v-api/v1/places/{placeId}/editModel |  |
| [**placeGetRentalItems()**](PlaceApi.md#placeGetRentalItems) | **GET** /v-api/v1/places/{placeId}/rentalItems |  |
| [**placeUpdatePlace()**](PlaceApi.md#placeUpdatePlace) | **PUT** /v-api/v1/places/{placeId} |  |
| [**placeValidateOpeningTimes()**](PlaceApi.md#placeValidateOpeningTimes) | **POST** /v-api/v1/places/{placeId}/validateOpeningTimes |  |


## `placeCreatePlace()`

```php
placeCreatePlace($place_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_input_model = new \WinterwerpItVerhuurClient\Model\PlaceInputModel(); // \WinterwerpItVerhuurClient\Model\PlaceInputModel

try {
    $apiInstance->placeCreatePlace($place_input_model);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeCreatePlace: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_input_model** | [**\WinterwerpItVerhuurClient\Model\PlaceInputModel**](../Model/PlaceInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeDeletePlace()`

```php
placeDeletePlace($place_id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_id = 56; // int

try {
    $apiInstance->placeDeletePlace($place_id);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeDeletePlace: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeGetAll()`

```php
placeGetAll(): \WinterwerpItVerhuurClient\Model\PlaceModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->placeGetAll();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeGetAll: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\WinterwerpItVerhuurClient\Model\PlaceModel[]**](../Model/PlaceModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeGetById()`

```php
placeGetById($place_id): \WinterwerpItVerhuurClient\Model\PlaceModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_id = 56; // int

try {
    $result = $apiInstance->placeGetById($place_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeGetById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\PlaceModel**](../Model/PlaceModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeGetByPermalink()`

```php
placeGetByPermalink($permalink): \WinterwerpItVerhuurClient\Model\PlaceModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$permalink = 'permalink_example'; // string

try {
    $result = $apiInstance->placeGetByPermalink($permalink);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeGetByPermalink: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **permalink** | **string**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\PlaceModel**](../Model/PlaceModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeGetForUpdatingById()`

```php
placeGetForUpdatingById($place_id): \WinterwerpItVerhuurClient\Model\PlaceModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_id = 56; // int

try {
    $result = $apiInstance->placeGetForUpdatingById($place_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeGetForUpdatingById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\PlaceModel**](../Model/PlaceModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeGetRentalItems()`

```php
placeGetRentalItems($place_id): \WinterwerpItVerhuurClient\Model\RentalItemModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_id = 56; // int

try {
    $result = $apiInstance->placeGetRentalItems($place_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeGetRentalItems: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemModel[]**](../Model/RentalItemModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeUpdatePlace()`

```php
placeUpdatePlace($place_id, $place_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_id = 56; // int
$place_input_model = new \WinterwerpItVerhuurClient\Model\PlaceInputModel(); // \WinterwerpItVerhuurClient\Model\PlaceInputModel

try {
    $apiInstance->placeUpdatePlace($place_id, $place_input_model);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeUpdatePlace: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_id** | **int**|  | |
| **place_input_model** | [**\WinterwerpItVerhuurClient\Model\PlaceInputModel**](../Model/PlaceInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `placeValidateOpeningTimes()`

```php
placeValidateOpeningTimes($place_id, $opening_time_validation_input_model): \WinterwerpItVerhuurClient\Model\OpeningTimeValidationResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\PlaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$place_id = 56; // int
$opening_time_validation_input_model = new \WinterwerpItVerhuurClient\Model\OpeningTimeValidationInputModel(); // \WinterwerpItVerhuurClient\Model\OpeningTimeValidationInputModel

try {
    $result = $apiInstance->placeValidateOpeningTimes($place_id, $opening_time_validation_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PlaceApi->placeValidateOpeningTimes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **place_id** | **int**|  | |
| **opening_time_validation_input_model** | [**\WinterwerpItVerhuurClient\Model\OpeningTimeValidationInputModel**](../Model/OpeningTimeValidationInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\OpeningTimeValidationResultModel**](../Model/OpeningTimeValidationResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

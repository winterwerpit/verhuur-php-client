# WinterwerpItVerhuurClient\AnnouncementApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**announcementCreateAnnouncement()**](AnnouncementApi.md#announcementCreateAnnouncement) | **POST** /v-api/v1/announcements |  |
| [**announcementDeleteAnnouncement()**](AnnouncementApi.md#announcementDeleteAnnouncement) | **DELETE** /v-api/v1/announcements/{announcementId} |  |
| [**announcementGetAnnouncement()**](AnnouncementApi.md#announcementGetAnnouncement) | **GET** /v-api/v1/announcements/{announcementId} |  |
| [**announcementGetAnnouncements()**](AnnouncementApi.md#announcementGetAnnouncements) | **GET** /v-api/v1/announcements |  |
| [**announcementSwitchVisibility()**](AnnouncementApi.md#announcementSwitchVisibility) | **PATCH** /v-api/v1/announcements/{announcementId}/visibility |  |
| [**announcementUpdateAnnouncement()**](AnnouncementApi.md#announcementUpdateAnnouncement) | **PUT** /v-api/v1/announcements/{announcementId} |  |


## `announcementCreateAnnouncement()`

```php
announcementCreateAnnouncement($announcement_input_model): \WinterwerpItVerhuurClient\Model\AnnouncementModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$announcement_input_model = new \WinterwerpItVerhuurClient\Model\AnnouncementInputModel(); // \WinterwerpItVerhuurClient\Model\AnnouncementInputModel

try {
    $result = $apiInstance->announcementCreateAnnouncement($announcement_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementCreateAnnouncement: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **announcement_input_model** | [**\WinterwerpItVerhuurClient\Model\AnnouncementInputModel**](../Model/AnnouncementInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\AnnouncementModel**](../Model/AnnouncementModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `announcementDeleteAnnouncement()`

```php
announcementDeleteAnnouncement($announcement_id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$announcement_id = 56; // int

try {
    $apiInstance->announcementDeleteAnnouncement($announcement_id);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementDeleteAnnouncement: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **announcement_id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `announcementGetAnnouncement()`

```php
announcementGetAnnouncement($announcement_id): \WinterwerpItVerhuurClient\Model\AnnouncementModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$announcement_id = 56; // int

try {
    $result = $apiInstance->announcementGetAnnouncement($announcement_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementGetAnnouncement: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **announcement_id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\AnnouncementModel**](../Model/AnnouncementModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `announcementGetAnnouncements()`

```php
announcementGetAnnouncements($only_active): \WinterwerpItVerhuurClient\Model\AnnouncementModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$only_active = True; // bool

try {
    $result = $apiInstance->announcementGetAnnouncements($only_active);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementGetAnnouncements: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **only_active** | **bool**|  | [optional] |

### Return type

[**\WinterwerpItVerhuurClient\Model\AnnouncementModel[]**](../Model/AnnouncementModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `announcementSwitchVisibility()`

```php
announcementSwitchVisibility($announcement_id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$announcement_id = 56; // int

try {
    $apiInstance->announcementSwitchVisibility($announcement_id);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementSwitchVisibility: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **announcement_id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `announcementUpdateAnnouncement()`

```php
announcementUpdateAnnouncement($announcement_id, $announcement_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AnnouncementApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$announcement_id = 56; // int
$announcement_input_model = new \WinterwerpItVerhuurClient\Model\AnnouncementInputModel(); // \WinterwerpItVerhuurClient\Model\AnnouncementInputModel

try {
    $apiInstance->announcementUpdateAnnouncement($announcement_id, $announcement_input_model);
} catch (Exception $e) {
    echo 'Exception when calling AnnouncementApi->announcementUpdateAnnouncement: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **announcement_id** | **int**|  | |
| **announcement_input_model** | [**\WinterwerpItVerhuurClient\Model\AnnouncementInputModel**](../Model/AnnouncementInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

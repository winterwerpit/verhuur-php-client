# WinterwerpItVerhuurClient\RentalItemApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**rentalItemCreateRentalItem()**](RentalItemApi.md#rentalItemCreateRentalItem) | **POST** /v-api/v1/rentalItems |  |
| [**rentalItemGetRentalItem()**](RentalItemApi.md#rentalItemGetRentalItem) | **GET** /v-api/v1/rentalItems/{id} |  |
| [**rentalItemGetRentalItemForEditing()**](RentalItemApi.md#rentalItemGetRentalItemForEditing) | **GET** /v-api/v1/rentalItems/{id}/editModel |  |
| [**rentalItemGetRentalItems()**](RentalItemApi.md#rentalItemGetRentalItems) | **GET** /v-api/v1/rentalItems |  |
| [**rentalItemUpdateRentalItem()**](RentalItemApi.md#rentalItemUpdateRentalItem) | **PUT** /v-api/v1/rentalItems/{id} |  |


## `rentalItemCreateRentalItem()`

```php
rentalItemCreateRentalItem($rental_item_input_model): \WinterwerpItVerhuurClient\Model\RentalItemModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rental_item_input_model = new \WinterwerpItVerhuurClient\Model\RentalItemInputModel(); // \WinterwerpItVerhuurClient\Model\RentalItemInputModel

try {
    $result = $apiInstance->rentalItemCreateRentalItem($rental_item_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemApi->rentalItemCreateRentalItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **rental_item_input_model** | [**\WinterwerpItVerhuurClient\Model\RentalItemInputModel**](../Model/RentalItemInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemModel**](../Model/RentalItemModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemGetRentalItem()`

```php
rentalItemGetRentalItem($id): \WinterwerpItVerhuurClient\Model\RentalItemModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->rentalItemGetRentalItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemApi->rentalItemGetRentalItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemModel**](../Model/RentalItemModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemGetRentalItemForEditing()`

```php
rentalItemGetRentalItemForEditing($id): \WinterwerpItVerhuurClient\Model\RentalItemModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->rentalItemGetRentalItemForEditing($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemApi->rentalItemGetRentalItemForEditing: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemModel**](../Model/RentalItemModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemGetRentalItems()`

```php
rentalItemGetRentalItems(): \WinterwerpItVerhuurClient\Model\RentalItemModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->rentalItemGetRentalItems();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemApi->rentalItemGetRentalItems: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\WinterwerpItVerhuurClient\Model\RentalItemModel[]**](../Model/RentalItemModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rentalItemUpdateRentalItem()`

```php
rentalItemUpdateRentalItem($id, $rental_item_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\RentalItemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$rental_item_input_model = new \WinterwerpItVerhuurClient\Model\RentalItemInputModel(); // \WinterwerpItVerhuurClient\Model\RentalItemInputModel

try {
    $apiInstance->rentalItemUpdateRentalItem($id, $rental_item_input_model);
} catch (Exception $e) {
    echo 'Exception when calling RentalItemApi->rentalItemUpdateRentalItem: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |
| **rental_item_input_model** | [**\WinterwerpItVerhuurClient\Model\RentalItemInputModel**](../Model/RentalItemInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

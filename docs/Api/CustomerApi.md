# WinterwerpItVerhuurClient\CustomerApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**customerRegister()**](CustomerApi.md#customerRegister) | **POST** /v-api/v1/customers |  |


## `customerRegister()`

```php
customerRegister($register_model): \WinterwerpItVerhuurClient\Model\LoginResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$register_model = new \WinterwerpItVerhuurClient\Model\RegisterModel(); // \WinterwerpItVerhuurClient\Model\RegisterModel

try {
    $result = $apiInstance->customerRegister($register_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerRegister: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **register_model** | [**\WinterwerpItVerhuurClient\Model\RegisterModel**](../Model/RegisterModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\LoginResultModel**](../Model/LoginResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

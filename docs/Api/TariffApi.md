# WinterwerpItVerhuurClient\TariffApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**tariffCalculateAmount()**](TariffApi.md#tariffCalculateAmount) | **POST** /v-api/v1/tariffs/{tariffId}/amount |  |
| [**tariffCalculateAmountByTariffName()**](TariffApi.md#tariffCalculateAmountByTariffName) | **POST** /v-api/v1/tariffs/{tariffName}/amount-by-tariff-name |  |
| [**tariffCreate()**](TariffApi.md#tariffCreate) | **POST** /v-api/v1/tariffs |  |
| [**tariffDeleteTariff()**](TariffApi.md#tariffDeleteTariff) | **DELETE** /v-api/v1/tariffs/{tariffId} |  |
| [**tariffGetAll()**](TariffApi.md#tariffGetAll) | **GET** /v-api/v1/tariffs |  |
| [**tariffGetTariff()**](TariffApi.md#tariffGetTariff) | **GET** /v-api/v1/tariffs/{tariffId} |  |
| [**tariffUpdateTariff()**](TariffApi.md#tariffUpdateTariff) | **PUT** /v-api/v1/tariffs/{tariffId} |  |


## `tariffCalculateAmount()`

```php
tariffCalculateAmount($tariff_id, $amount_calculation_input_model): \WinterwerpItVerhuurClient\Model\AmountCalculationResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tariff_id = 56; // int
$amount_calculation_input_model = new \WinterwerpItVerhuurClient\Model\AmountCalculationInputModel(); // \WinterwerpItVerhuurClient\Model\AmountCalculationInputModel

try {
    $result = $apiInstance->tariffCalculateAmount($tariff_id, $amount_calculation_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffCalculateAmount: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tariff_id** | **int**|  | |
| **amount_calculation_input_model** | [**\WinterwerpItVerhuurClient\Model\AmountCalculationInputModel**](../Model/AmountCalculationInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\AmountCalculationResultModel**](../Model/AmountCalculationResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `tariffCalculateAmountByTariffName()`

```php
tariffCalculateAmountByTariffName($tariff_name, $amount_calculation_input_model): \WinterwerpItVerhuurClient\Model\AmountCalculationResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tariff_name = 'tariff_name_example'; // string
$amount_calculation_input_model = new \WinterwerpItVerhuurClient\Model\AmountCalculationInputModel(); // \WinterwerpItVerhuurClient\Model\AmountCalculationInputModel

try {
    $result = $apiInstance->tariffCalculateAmountByTariffName($tariff_name, $amount_calculation_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffCalculateAmountByTariffName: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tariff_name** | **string**|  | |
| **amount_calculation_input_model** | [**\WinterwerpItVerhuurClient\Model\AmountCalculationInputModel**](../Model/AmountCalculationInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\AmountCalculationResultModel**](../Model/AmountCalculationResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `tariffCreate()`

```php
tariffCreate($tariff_input_model): \WinterwerpItVerhuurClient\Model\TariffModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tariff_input_model = new \WinterwerpItVerhuurClient\Model\TariffInputModel(); // \WinterwerpItVerhuurClient\Model\TariffInputModel

try {
    $result = $apiInstance->tariffCreate($tariff_input_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tariff_input_model** | [**\WinterwerpItVerhuurClient\Model\TariffInputModel**](../Model/TariffInputModel.md)|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\TariffModel**](../Model/TariffModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `tariffDeleteTariff()`

```php
tariffDeleteTariff($tariff_id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tariff_id = 56; // int

try {
    $apiInstance->tariffDeleteTariff($tariff_id);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffDeleteTariff: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tariff_id** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `tariffGetAll()`

```php
tariffGetAll(): \WinterwerpItVerhuurClient\Model\TariffModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->tariffGetAll();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffGetAll: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\WinterwerpItVerhuurClient\Model\TariffModel[]**](../Model/TariffModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `tariffGetTariff()`

```php
tariffGetTariff($tariff_id): \WinterwerpItVerhuurClient\Model\TariffModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tariff_id = 56; // int

try {
    $result = $apiInstance->tariffGetTariff($tariff_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffGetTariff: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tariff_id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\TariffModel**](../Model/TariffModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `tariffUpdateTariff()`

```php
tariffUpdateTariff($tariff_id, $tariff_input_model)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\TariffApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tariff_id = 56; // int
$tariff_input_model = new \WinterwerpItVerhuurClient\Model\TariffInputModel(); // \WinterwerpItVerhuurClient\Model\TariffInputModel

try {
    $apiInstance->tariffUpdateTariff($tariff_id, $tariff_input_model);
} catch (Exception $e) {
    echo 'Exception when calling TariffApi->tariffUpdateTariff: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **tariff_id** | **int**|  | |
| **tariff_input_model** | [**\WinterwerpItVerhuurClient\Model\TariffInputModel**](../Model/TariffInputModel.md)|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

# WinterwerpItVerhuurClient\QueuedMailApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**queuedMailGetQueuedMail()**](QueuedMailApi.md#queuedMailGetQueuedMail) | **GET** /v-api/v1/users/{userId}/queuedMails/{queuedMailId} |  |
| [**queuedMailGetQueuedMailAttachment()**](QueuedMailApi.md#queuedMailGetQueuedMailAttachment) | **GET** /v-api/v1/users/{userId}/queuedMails/{queuedMailId}/attachments/{attachmentId} |  |
| [**queuedMailGetQueuedMailAttachments()**](QueuedMailApi.md#queuedMailGetQueuedMailAttachments) | **GET** /v-api/v1/users/{userId}/queuedMails/{queuedMailId}/attachments |  |
| [**queuedMailGetQueuedMailsOverview()**](QueuedMailApi.md#queuedMailGetQueuedMailsOverview) | **GET** /v-api/v1/users/{userId}/queuedMails |  |


## `queuedMailGetQueuedMail()`

```php
queuedMailGetQueuedMail($queued_mail_id, $user_id): \WinterwerpItVerhuurClient\Model\QueuedMailModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\QueuedMailApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$queued_mail_id = 56; // int
$user_id = 'user_id_example'; // string

try {
    $result = $apiInstance->queuedMailGetQueuedMail($queued_mail_id, $user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueuedMailApi->queuedMailGetQueuedMail: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **queued_mail_id** | **int**|  | |
| **user_id** | **string**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\QueuedMailModel**](../Model/QueuedMailModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queuedMailGetQueuedMailAttachment()`

```php
queuedMailGetQueuedMailAttachment($attachment_id, $user_id, $queued_mail_id)
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\QueuedMailApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$attachment_id = 56; // int
$user_id = 'user_id_example'; // string
$queued_mail_id = 'queued_mail_id_example'; // string

try {
    $apiInstance->queuedMailGetQueuedMailAttachment($attachment_id, $user_id, $queued_mail_id);
} catch (Exception $e) {
    echo 'Exception when calling QueuedMailApi->queuedMailGetQueuedMailAttachment: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **attachment_id** | **int**|  | |
| **user_id** | **string**|  | |
| **queued_mail_id** | **string**|  | |

### Return type

void (empty response body)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queuedMailGetQueuedMailAttachments()`

```php
queuedMailGetQueuedMailAttachments($queued_mail_id, $user_id): \WinterwerpItVerhuurClient\Model\QueuedMailAttachmentModel[]
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\QueuedMailApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$queued_mail_id = 56; // int
$user_id = 'user_id_example'; // string

try {
    $result = $apiInstance->queuedMailGetQueuedMailAttachments($queued_mail_id, $user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueuedMailApi->queuedMailGetQueuedMailAttachments: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **queued_mail_id** | **int**|  | |
| **user_id** | **string**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\QueuedMailAttachmentModel[]**](../Model/QueuedMailAttachmentModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queuedMailGetQueuedMailsOverview()`

```php
queuedMailGetQueuedMailsOverview($user_id): \WinterwerpItVerhuurClient\Model\QueuedMailOverviewModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\QueuedMailApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$user_id = 56; // int

try {
    $result = $apiInstance->queuedMailGetQueuedMailsOverview($user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueuedMailApi->queuedMailGetQueuedMailsOverview: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **user_id** | **int**|  | |

### Return type

[**\WinterwerpItVerhuurClient\Model\QueuedMailOverviewModel**](../Model/QueuedMailOverviewModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

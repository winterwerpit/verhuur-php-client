# WinterwerpItVerhuurClient\AuthenticationApi

All URIs are relative to http://localhost, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**authenticationAuthenticate()**](AuthenticationApi.md#authenticationAuthenticate) | **POST** /v-api/v1/authentication |  |


## `authenticationAuthenticate()`

```php
authenticationAuthenticate($login_model): \WinterwerpItVerhuurClient\Model\LoginResultModel
```



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: JWT
$config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = WinterwerpItVerhuurClient\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new WinterwerpItVerhuurClient\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$login_model = new \WinterwerpItVerhuurClient\Model\LoginModel(); // \WinterwerpItVerhuurClient\Model\LoginModel

try {
    $result = $apiInstance->authenticationAuthenticate($login_model);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->authenticationAuthenticate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **login_model** | [**\WinterwerpItVerhuurClient\Model\LoginModel**](../Model/LoginModel.md)|  | [optional] |

### Return type

[**\WinterwerpItVerhuurClient\Model\LoginResultModel**](../Model/LoginResultModel.md)

### Authorization

[JWT](../../README.md#JWT)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

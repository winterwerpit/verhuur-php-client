# # PlaceInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**permalink** | **string** |  | [optional]
**address** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**city** | **string** |  | [optional]
**phone** | **string** |  | [optional]
**email** | **string** |  | [optional]
**description** | **string** |  | [optional]
**visible** | **bool** |  | [optional]
**extra_contract_text** | **string** |  | [optional]
**place_name_on_contract** | **string** |  | [optional]
**place_category** | [**\WinterwerpItVerhuurClient\Model\PlaceCategory**](PlaceCategory.md) |  | [optional]
**minimum_amount_of_hours_for_booking** | **int** |  | [optional]
**default_media_attachment_ids** | **int[]** |  | [optional]
**media_ids** | **int[]** |  | [optional]
**opening_times** | [**\WinterwerpItVerhuurClient\Model\OpeningTimeInputModel[]**](OpeningTimeInputModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # TariffModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**tariff_name** | **string** |  | [optional]
**round_amount_up** | **bool** |  | [optional]
**tariff_lines** | [**\WinterwerpItVerhuurClient\Model\TariffLineModel[]**](TariffLineModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

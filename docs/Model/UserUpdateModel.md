# # UserUpdateModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** |  | [optional]
**last_name** | **string** |  | [optional]
**street** | **string** |  | [optional]
**number** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**city** | **string** |  | [optional]
**phone** | **string** |  | [optional]
**email** | **string** |  | [optional]
**newsletter** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

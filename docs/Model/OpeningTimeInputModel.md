# # OpeningTimeInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day_of_week** | [**\WinterwerpItVerhuurClient\Model\OpeningTimeModelDayOfWeek**](OpeningTimeModelDayOfWeek.md) |  | [optional]
**date** | **\DateTime** |  | [optional]
**open_from** | **string** |  | [optional]
**open_until** | **string** |  | [optional]
**returnable_until** | **string** |  | [optional]
**closed** | **bool** |  | [optional]
**open_on_request** | **bool** |  | [optional]
**open_whole_day** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

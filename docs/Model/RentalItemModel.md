# # RentalItemModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**identifier** | **string** |  | [optional]
**visible** | **bool** |  | [optional]
**bookable_by_customer** | **bool** |  | [optional]
**quantity** | **int** |  | [optional]
**description** | **string** |  | [optional]
**default_media_attachment_ids** | **int[]** |  | [optional]
**extra_ids** | **int[]** |  | [optional]
**media_ids** | **int[]** |  | [optional]
**media_items** | [**\WinterwerpItVerhuurClient\Model\MediaModel[]**](MediaModel.md) |  | [optional]
**place_id** | **int** |  | [optional]
**tariff_id** | **int** |  | [optional]
**rental_item_type_id** | **int** |  | [optional]
**rental_item_type** | [**\WinterwerpItVerhuurClient\Model\RentalItemModelRentalItemType**](RentalItemModelRentalItemType.md) |  | [optional]
**metadata** | [**\WinterwerpItVerhuurClient\Model\RentalItemMetadataModel[]**](RentalItemMetadataModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

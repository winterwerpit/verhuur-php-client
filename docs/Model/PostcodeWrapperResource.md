# # PostcodeWrapperResource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **string** |  | [optional]
**postcode** | **string** |  | [optional]
**town** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

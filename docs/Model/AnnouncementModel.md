# # AnnouncementModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**visible** | **bool** |  | [optional]
**announcement_type** | [**\WinterwerpItVerhuurClient\Model\AnnouncementModelAnnouncementType**](AnnouncementModelAnnouncementType.md) |  | [optional]
**show_from** | **\DateTime** |  | [optional]
**show_to** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

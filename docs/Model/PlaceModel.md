# # PlaceModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**permalink** | **string** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**name** | **string** |  | [optional]
**address** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**city** | **string** |  | [optional]
**phone** | **string** |  | [optional]
**email** | **string** |  | [optional]
**description** | **string** |  | [optional]
**extra_contract_text** | **string** |  | [optional]
**place_name_on_contract** | **string** |  | [optional]
**place_category** | [**\WinterwerpItVerhuurClient\Model\PlaceCategory**](PlaceCategory.md) |  | [optional]
**minimum_amount_of_hours_for_booking** | **int** |  | [optional]
**default_media_attachment_ids** | **int[]** |  | [optional]
**media_ids** | **int[]** |  | [optional]
**media_items** | [**\WinterwerpItVerhuurClient\Model\MediaModel[]**](MediaModel.md) |  | [optional]
**opening_times** | [**\WinterwerpItVerhuurClient\Model\OpeningTimeModel[]**](OpeningTimeModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # MetadataInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** |  | [optional]
**name** | **string** |  | [optional]
**language** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

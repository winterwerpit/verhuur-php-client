# # RentalItemInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional]
**visible** | **bool** |  | [optional]
**bookable_by_customer** | **bool** |  | [optional]
**quantity** | **int** |  | [optional]
**description** | **string** |  | [optional]
**comment** | **string** |  | [optional]
**place_id** | **int** |  | [optional]
**tariff_id** | **int** |  | [optional]
**rental_item_type_id** | **int** |  | [optional]
**metadata** | [**array<string,\WinterwerpItVerhuurClient\Model\MetadataInputModel>**](MetadataInputModel.md) |  | [optional]
**default_media_attachment_ids** | **int[]** |  | [optional]
**media_ids** | **int[]** |  | [optional]
**extra_ids** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

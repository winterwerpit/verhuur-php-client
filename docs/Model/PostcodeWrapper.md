# # PostcodeWrapper

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | [optional]
**resource** | [**\WinterwerpItVerhuurClient\Model\PostcodeWrapperResource**](PostcodeWrapperResource.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

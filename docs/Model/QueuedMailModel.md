# # QueuedMailModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**subject** | **string** |  | [optional]
**body** | **string** |  | [optional]
**is_body_html** | **bool** |  | [optional]
**from_name** | **string** |  | [optional]
**from_email** | **string** |  | [optional]
**to** | **string** |  | [optional]
**reply_to** | **string** |  | [optional]
**cc** | **string** |  | [optional]
**bcc** | **string** |  | [optional]
**error** | **string** |  | [optional]
**mail_send_status** | [**\WinterwerpItVerhuurClient\Model\MailSendStatusType**](MailSendStatusType.md) |  | [optional]
**mail_send_status_text** | **string** |  | [optional]
**retry_counter** | **int** |  | [optional]
**next_try_again_date** | **\DateTime** |  | [optional]
**email_send_date** | **\DateTime** |  | [optional]
**user_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

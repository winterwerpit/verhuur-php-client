# # QueuedMailOverviewModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**subject** | **string** |  | [optional]
**to** | **string** |  | [optional]
**mail_send_status** | [**\WinterwerpItVerhuurClient\Model\MailSendStatusType**](MailSendStatusType.md) |  | [optional]
**email_send_date** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

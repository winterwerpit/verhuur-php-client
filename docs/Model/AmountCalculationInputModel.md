# # AmountCalculationInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **\DateTime** |  | [optional]
**to** | **\DateTime** |  | [optional]
**extras** | [**\WinterwerpItVerhuurClient\Model\BookingExtraModel[]**](BookingExtraModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # CreateBookingResultModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**can_retrieve_right_away** | **bool** |  | [optional]
**booking** | [**\WinterwerpItVerhuurClient\Model\CreateBookingResultModelBooking**](CreateBookingResultModelBooking.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

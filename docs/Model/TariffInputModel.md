# # TariffInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariff_name** | **string** |  | [optional]
**round_amount_up** | **bool** |  | [optional]
**tariff_lines** | [**\WinterwerpItVerhuurClient\Model\TariffLineInputModel[]**](TariffLineInputModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

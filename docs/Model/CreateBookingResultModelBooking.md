# # CreateBookingResultModelBooking

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**start_timestamp** | **\DateTime** |  | [optional]
**end_timestamp** | **\DateTime** |  | [optional]
**booking_timestamp** | **\DateTime** |  | [optional]
**original_start_timestamp** | **\DateTime** |  | [optional]
**original_end_timestamp** | **\DateTime** |  | [optional]
**booking_status** | [**\WinterwerpItVerhuurClient\Model\BookingStatusType**](BookingStatusType.md) |  | [optional]
**code** | **string** |  | [optional]
**source** | [**\WinterwerpItVerhuurClient\Model\BookingSourceType**](BookingSourceType.md) |  | [optional]
**booking_activation_code** | **string** |  | [optional]
**rental_item_id** | **int** |  | [optional]
**place_id** | **int** |  | [optional]
**rental_item_identifier** | **string** |  | [optional]
**rental_item_type_name** | **string** |  | [optional]
**place_name** | **string** |  | [optional]
**place_permalink** | **string** |  | [optional]
**last_name** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

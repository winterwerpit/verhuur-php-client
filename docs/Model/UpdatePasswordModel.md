# # UpdatePasswordModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_password** | **string** |  | [optional]
**new_password_repeat** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # BookingInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**place_id** | **int** |  | [optional]
**rental_item_id** | **int** |  | [optional]
**start_timestamp** | **\DateTime** |  | [optional]
**end_timestamp** | **\DateTime** |  | [optional]
**newsletter** | **bool** |  | [optional]
**metadata** | **array<string,string>** |  | [optional]
**extras** | [**\WinterwerpItVerhuurClient\Model\BookingExtra[]**](BookingExtra.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # AvailabilityModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rental_item_id** | **int** |  | [optional]
**is_blocking** | **bool** |  | [optional]
**available_slots** | [**\WinterwerpItVerhuurClient\Model\DateRange[]**](DateRange.md) |  | [optional]
**occupied_slots** | [**\WinterwerpItVerhuurClient\Model\DateRange[]**](DateRange.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # ExtraModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**name** | **string** |  | [optional]
**description** | **string** |  | [optional]
**deposit** | **float** |  | [optional]
**booking_can_have_multiple** | **bool** |  | [optional]
**types** | [**\WinterwerpItVerhuurClient\Model\RentalItemObjectType[]**](RentalItemObjectType.md) |  | [optional]
**tariff** | [**\WinterwerpItVerhuurClient\Model\ExtraModelTariff**](ExtraModelTariff.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

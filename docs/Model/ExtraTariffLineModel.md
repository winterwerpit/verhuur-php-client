# # ExtraTariffLineModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time_from** | **float** |  | [optional]
**time_to** | **float** |  | [optional]
**amount** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

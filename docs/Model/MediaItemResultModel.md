# # MediaItemResultModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**description** | **string** |  | [optional]
**filename** | **string** |  | [optional]
**mime_type** | **string** |  | [optional]
**public_url** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # RegisterModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** |  | [optional]
**last_name** | **string** |  | [optional]
**email** | **string** |  | [optional]
**email_repeat** | **string** |  | [optional]
**phone** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**number** | **string** |  | [optional]
**street** | **string** |  | [optional]
**city** | **string** |  | [optional]
**agreed_to_terms** | **bool** |  | [optional]
**newsletter** | **bool** |  | [optional]
**place_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

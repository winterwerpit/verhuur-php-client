# # RentalItemModelRentalItemType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**key** | **string** |  | [optional]
**description** | **string** |  | [optional]
**name** | **string** |  | [optional]
**type** | [**\WinterwerpItVerhuurClient\Model\RentalItemObjectType**](RentalItemObjectType.md) |  | [optional]
**default_media_attachment_ids** | **int[]** |  | [optional]
**media_ids** | **int[]** |  | [optional]
**media_items** | [**\WinterwerpItVerhuurClient\Model\MediaModel[]**](MediaModel.md) |  | [optional]
**metadata** | [**\WinterwerpItVerhuurClient\Model\RentalItemTypeMetadataModel[]**](RentalItemTypeMetadataModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

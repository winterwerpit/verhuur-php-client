# # CalculateAvailabilityInputModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rental_item_ids** | **int[]** |  | [optional]
**from** | **\DateTime** |  | [optional]
**to** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

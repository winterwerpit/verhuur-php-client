# # DeskDeviceModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**last_seen_date** | **\DateTime** |  | [optional]
**device_name** | **string** |  | [optional]
**app_version** | **string** |  | [optional]
**is_active** | **bool** |  | [optional]
**place_id** | **int** |  | [optional]
**place_name** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

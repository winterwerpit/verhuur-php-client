# # MetadataDescriptionModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** |  | [optional]
**name** | **string** |  | [optional]
**required** | **bool** |  | [optional]
**type** | [**\WinterwerpItVerhuurClient\Model\MetadataFieldType**](MetadataFieldType.md) |  | [optional]
**values** | [**\WinterwerpItVerhuurClient\Model\MetadataListValueModel[]**](MetadataListValueModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

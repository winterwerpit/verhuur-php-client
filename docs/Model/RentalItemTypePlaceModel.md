# # RentalItemTypePlaceModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**place_name** | **string** |  | [optional]
**place_permalink** | **string** |  | [optional]
**identifiers** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

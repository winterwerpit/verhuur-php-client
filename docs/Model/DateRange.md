# # DateRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **\DateTime** |  | [optional]
**end** | **\DateTime** |  | [optional]
**difference_in_seconds** | **float** |  | [optional]
**difference_in_hours** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

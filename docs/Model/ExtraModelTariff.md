# # ExtraModelTariff

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fixed_amount** | **float** |  | [optional]
**tariff_lines** | [**\WinterwerpItVerhuurClient\Model\ExtraTariffLineModel[]**](ExtraTariffLineModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

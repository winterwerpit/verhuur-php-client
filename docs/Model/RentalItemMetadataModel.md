# # RentalItemMetadataModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**key** | **string** |  | [optional]
**name** | **string** |  | [optional]
**metadata_field_type** | [**\WinterwerpItVerhuurClient\Model\MetadataFieldType**](MetadataFieldType.md) |  | [optional]
**value** | **string** |  | [optional]
**language** | **string** |  | [optional]
**rental_item_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

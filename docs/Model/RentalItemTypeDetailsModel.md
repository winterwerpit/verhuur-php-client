# # RentalItemTypeDetailsModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rental_item_type** | [**\WinterwerpItVerhuurClient\Model\RentalItemModelRentalItemType**](RentalItemModelRentalItemType.md) |  | [optional]
**rental_item_type_places** | [**\WinterwerpItVerhuurClient\Model\RentalItemTypePlaceModel[]**](RentalItemTypePlaceModel.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

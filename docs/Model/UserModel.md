# # UserModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**user_name** | **string** |  | [optional]
**first_name** | **string** |  | [optional]
**last_name** | **string** |  | [optional]
**street** | **string** |  | [optional]
**number** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**city** | **string** |  | [optional]
**phone** | **string** |  | [optional]
**email** | **string** |  | [optional]
**place_id** | **int** |  | [optional]
**newsletter** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

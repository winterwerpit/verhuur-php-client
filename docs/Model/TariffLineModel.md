# # TariffLineModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**created** | **\DateTime** |  | [optional]
**updated** | **\DateTime** |  | [optional]
**label** | **string** |  | [optional]
**amount** | **float** |  | [optional]
**time_from** | **float** |  | [optional]
**time_to** | **float** |  | [optional]
**from_day** | [**\WinterwerpItVerhuurClient\Model\OpeningTimeModelDayOfWeek**](OpeningTimeModelDayOfWeek.md) |  | [optional]
**to_day** | [**\WinterwerpItVerhuurClient\Model\OpeningTimeModelDayOfWeek**](OpeningTimeModelDayOfWeek.md) |  | [optional]
**from_hour** | **string** |  | [optional]
**to_hour** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

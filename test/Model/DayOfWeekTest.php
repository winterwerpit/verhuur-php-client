<?php
/**
 * DayOfWeekTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  WinterwerpItVerhuurClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Verhuur API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.4.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace WinterwerpItVerhuurClient\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * DayOfWeekTest Class Doc Comment
 *
 * @category    Class
 * @description 
 * @package     WinterwerpItVerhuurClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class DayOfWeekTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "DayOfWeek"
     */
    public function testDayOfWeek()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }
}
